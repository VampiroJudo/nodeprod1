const express = require('express');
const chalk = require('chalk');
const debug = require('debug')('app');
const morgan = require('morgan');
const path = require('path');
const MongoClient = require('mongodb').MongoClient

const app = express();
const port = process.env.PORT || 3000;


app.use(morgan('tiny'));
app.use(express.static(path.join(__dirname, '/public/')));
app.use('/css', express.static(path.join(__dirname, 'node_modules/bootstrap/dist/css')));
app.use('/js', express.static(path.join(__dirname, 'node_modules/bootstrap/dist/js')));
app.use('/js', express.static(path.join(__dirname, 'node_modules/jquery/dist')));

// Initial routes
app.get(`/`, (req, res) => {
	res.sendFile(path.join(__dirname, '/views/index.js'));
});
app.listen(`${port}`, (req, res) => {
	debug(`Server connected successsfully on Port: ${chalk.green(`${port}`)}`);
});

// MongoDB connection
const uri = "mongodb+srv://Conley:<Gibson7301>@cluster0.9lxbq.mongodb.net/<MongNode>?retryWrites=true&w=majority";
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
client.connect(err => {
  const collection = client.db("MongNode").collection("uri");
  // perform actions on the collection object
  client.close();
});